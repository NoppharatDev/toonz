import { PicuEntity } from 'src/picu/picu.entity';
import { CvpEntity } from 'src/cvp/cvp.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { UserRole } from './role.emum';

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  cmuitaccount_name: string;

  @Column()
  cmuitaccount: string;

  @Column()
  student_id: string;

  @Column()
  prename_id: string;

  @Column()
  prename_TH: string;

  @Column()
  prename_EN: string;

  @Column()
  firstname_TH: string;

  @Column()
  firstname_EN: string;

  @Column()
  lastname_TH: string;

  @Column()
  lastname_EN: string;

  @Column()
  organization_code: string;

  @Column()
  organization_name_TH: string;

  @Column()
  organization_name_EN: string;

  @Column()
  itaccounttype_id: string;

  @Column()
  itaccounttype_TH: string;

  @Column()
  itaccounttype_EN: string;

  @Column({ default: UserRole.USER })
  role: UserRole;

  @Column({ default: 0 })
  countRound: number;

  @Column({ default: true })
  is_active: boolean;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    nullable: true,
  })
  last_login: Date;

  @Column({ type: 'timestamp', nullable: true })
  last_logout: Date;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @OneToMany((type) => CvpEntity, (scoreEntity) => scoreEntity.user, {
    eager: false,
  })
  cvps: CvpEntity[];

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @OneToMany((type) => PicuEntity, (picuEntity) => picuEntity.user, {
    eager: false,
  })
  picus: PicuEntity[];
}
