export class GameOverReturnDto {
  success: boolean;

  message: string;
}
