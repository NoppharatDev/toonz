export class PicuOverDto {
  student_id: string;

  envis: Array<number>;

  stepTimes: Array<number>;

  resultTime: number;

  mode: number;
}
