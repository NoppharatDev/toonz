export class LabOverDto {
  student_id: string;

  labs: Array<number>;

  resultTime: number;

  mode: number;
}
