export class GameOverDto {
  student_id: string;

  scores: Array<number>;

  stepTimes: Array<number>;

  resultTime: number;

  mode: number;
}
