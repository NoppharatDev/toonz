// {
//     cmuitaccount_name: 'katanchalee_s'
//     cmuitaccount: 'katanchalee_s@cmu.ac.th'
//     student_id: '561532274'
//     prename_id: 'MS'
//     prename_TH: 'นางสาว'
//     prename_EN: 'Miss'
//     firstname_TH: 'กตัญชลี'
//     firstname_EN: 'KATANCHALEE'
//     lastname_TH: 'แสงรัก'
//     lastname_EN: 'SAENGRAK'
//     organization_code: '15'
//     organization_name_TH: 'คณะบริหารธุรกิจ'
//     organization_name_EN: 'Faculty of Business Administration'
//     itaccounttype_id: 'AlumAcc'
//     itaccounttype_TH: 'นักศึกษาเก่า'
//     itaccounttype_EN: 'Alumni Account'
//     }

export class CreateUserDto {
  cmuitaccount_name: string;

  cmuitaccount: string;

  student_id: string;

  prename_id: string;

  prename_TH: string;

  prename_EN: string;

  firstname_TH: string;

  firstname_EN: string;

  lastname_TH: string;

  lastname_EN: string;

  organization_code: string;

  organization_name_TH: string;

  organization_name_EN: string;

  itaccounttype_id: string;

  itaccounttype_TH: string;

  itaccounttype_EN: string;
}
