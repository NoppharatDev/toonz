import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Query,
  Param,
} from '@nestjs/common';
import { ReturnData } from 'src/libs/dto/returndata.dto';
import { CreateUserReturnDto } from './dto/create-user-return.dto';
import { UserService } from './user.service';

@Controller()
export class UserController {
  constructor(private userService: UserService) {}

  @Get('auth/register')
  register() {
    return this.userService.register();
  }

  @Get('auth/callback')
  async registerCallback(
    @Query('code') code: string,
    @Query('error') error: string,
  ): Promise<CreateUserReturnDto> {
    if (error) {
      throw new BadRequestException();
    }

    return this.userService.registerCallback(code);
  }

  // Users
  @Get('users')
  async getAllusers() {
    return this.userService.getAllusers();
  }

  @Get('users/:id')
  async getUserById(@Param('id') id: string) {
    return this.userService.getUserById(id);
  }

  // Auth
  @Post('auth/login')
  async loginByStudentId(
    @Body('student_id') student_id: string,
  ): Promise<CreateUserReturnDto> {
    return this.userService.loginByStudentId(student_id);
  }

  @Post('auth/logout')
  async logoutByStudentId(
    @Body('student_id') studentId: string,
  ): Promise<ReturnData> {
    return this.userService.logout(studentId);
  }
}
