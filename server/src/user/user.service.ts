import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom, map } from 'rxjs';
import { CreateUserDto } from './dto/create-user.dto';
import { ReturnData } from 'src/libs/dto/returndata.dto';
import { CreateUserReturnDto } from './dto/create-user-return.dto';
import { CvpRepository } from 'src/cvp/cvp.repository';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository) private userRepo: UserRepository,
    @InjectRepository(CvpRepository) private cvpRepo: CvpRepository,
    private httpService: HttpService,
  ) {}

  register(): string {
    return `https://oauth.cmu.ac.th/v1/Authorize.aspx?response_type=code&client_id=${process.env.CLIENT_ID}&redirect_uri=${process.env.REDIRECT_CODE_URL}&scope=cmuitaccount.basicinfo&state=cm`;
  }

  async registerCallback(code: string): Promise<CreateUserReturnDto> {
    try {
      const token = await lastValueFrom(
        this.httpService
          .post(
            `https://oauth.cmu.ac.th/v1/GetToken.aspx?code=${code}&redirect_uri=${process.env.REDIRECT_CODE_URL}&client_id=${process.env.CLIENT_ID}&client_secret=${process.env.CLIENT_SECRET}&grant_type=authorization_code`,
          )
          .pipe(map((res) => res.data)),
      );

      const user = await lastValueFrom(
        this.httpService.get(
          'https://misapi.cmu.ac.th/cmuitaccount/v1/api/cmuitaccount/basicinfo',
          {
            headers: { Authorization: `Bearer ${token.access_token}` },
          },
        ),
      );

      await this.createUser(user.data);

      // return newUser;
      return {
        success: true,
        message: 'register successfully',
      };
    } catch (error) {
      // throw new BadRequestException(error.message);
      return {
        success: false,
        message: error.message,
      };
    }
  }

  async getAllusers(): Promise<any> {
    try {
      const users = await this.userRepo.find({ relations: ['picus', 'cvps'] });
      // console.log('users: ', users);
      return { users };
    } catch (error) {
      return {
        success: false,
        message: error.message,
      };
    }
  }

  async getUserById(id: string): Promise<any> {
    try {
      const user = await this.userRepo.findOneOrFail({
        where: { id: id },
        relations: ['picus', 'cvps'],
      });

      // const scores = await this.scoreRepo.find({});

      return { user };
    } catch (error) {
      return {
        success: false,
        message: error.message,
      };
    }
  }

  async createUser(createUserDto: CreateUserDto): Promise<CreateUserReturnDto> {
    const {
      cmuitaccount_name,
      cmuitaccount,
      student_id,
      prename_id,
      prename_TH,
      prename_EN,
      firstname_TH,
      firstname_EN,
      lastname_TH,
      lastname_EN,
      organization_code,
      organization_name_TH,
      organization_name_EN,
      itaccounttype_id,
      itaccounttype_TH,
      itaccounttype_EN,
    } = createUserDto;

    try {
      const found = await this.userRepo.findOne({
        where: { student_id },
      });

      if (found) {
        throw new BadRequestException('user already exist');
      } else {
        const user = this.userRepo.create({
          cmuitaccount_name,
          cmuitaccount,
          student_id,
          prename_id,
          prename_TH,
          prename_EN,
          firstname_TH,
          firstname_EN,
          lastname_TH,
          lastname_EN,
          organization_code,
          organization_name_TH,
          organization_name_EN,
          itaccounttype_id,
          itaccounttype_TH,
          itaccounttype_EN,
        });

        await this.userRepo.save(user);

        // return user;
        return {
          success: true,
          message: 'register successfully',
        };
      }
    } catch (error) {
      // throw new BadRequestException(error.message);
      return {
        success: false,
        message: error.message,
      };
    }
  }

  async loginByStudentId(studentId: string): Promise<CreateUserReturnDto> {
    if (!studentId) {
      return {
        success: false,
        message: 'โปรดระบุ student id',
      };
    }

    try {
      const user = await this.userRepo.findOneOrFail({ student_id: studentId });
      user.last_login = new Date();

      await this.userRepo.save(user);

      return {
        success: true,
        message: '',
      };
    } catch (error) {
      return {
        success: false,
        message: error.message,
      };
    }
  }


  async logout(studentId: string): Promise<ReturnData> {
    const user = await this.userRepo.findOneOrFail({ student_id: studentId });
    user.last_logout = new Date();
    await this.userRepo.save(user);

    return {
      message: 'success',
      statusCode: 200,
    };
  }

  async findByStudentId(studentId: string): Promise<any> {
    const found = await this.userRepo.findOne({
      where: { student_id: studentId },
    });

    if (!found) {
      throw new NotFoundException();
    }

    return found;
  }
}
