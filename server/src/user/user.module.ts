import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from './user.controller';
import { UserRepository } from './user.repository';
import { UserService } from './user.service';
import { CvpModule } from 'src/cvp/cvp.module';
import { CvpRepository } from 'src/cvp/cvp.repository';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forFeature([UserRepository, CvpRepository]),
    HttpModule,
    CvpModule,
  ],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
