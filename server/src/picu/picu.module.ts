import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/user/user.repository';
import { PicuController } from './picu.controller';
import { PicuRepository } from './picu.repository';
import { PicuService } from './picu.service';

@Module({
  imports: [TypeOrmModule.forFeature([PicuRepository, UserRepository])],
  controllers: [PicuController],
  providers: [PicuService],
  exports: [PicuService],
})
export class PicuModule {}
