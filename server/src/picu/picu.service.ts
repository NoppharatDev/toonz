import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GameOverReturnDto } from 'src/user/dto/game-over-return.dto';
import { PicuOverDto } from 'src/user/dto/picu-over.dto';
import { UserRepository } from 'src/user/user.repository';
import { PicuEntity } from './picu.entity';
import { PicuRepository } from './picu.repository';

@Injectable()
export class PicuService {
  constructor(
    @InjectRepository(PicuRepository) private picuRepo: PicuRepository,
    @InjectRepository(UserRepository) private userRepo: UserRepository,
  ) {}

  async createNewPicu(picu: PicuEntity): Promise<PicuEntity> {
    try {
      const newPicu = this.picuRepo.create(picu);

      await this.picuRepo.save(newPicu);

      return newPicu;
    } catch (error) {
      throw new BadRequestException();
    }
  }

  async getAllPicuScores(): Promise<any> {
    try {
      const picuScores = await this.picuRepo.find({ relations: ['user'] });

      return { picuScores };
    } catch (error) {
      return {
        success: false,
        message: error.message,
      };
    }
  }

  async getPicuScoreById(id: string): Promise<any> {
    try {
      const cvpScore = await this.picuRepo.findOneOrFail({
        where: { id: id },
        // relations: ['user'],
      });

      return { cvpScore };
    } catch (error) {
      return {
        success: false,
        message: error.message,
      };
    }
  }

  async picuOver(picuOverDto: PicuOverDto): Promise<GameOverReturnDto> {
    const { envis, stepTimes, student_id, resultTime, mode } = picuOverDto;

    if (!student_id) {
      return {
        success: false,
        message: 'โปรดระบุ student id',
      };
    }

    try {
      const user = await this.userRepo.findOneOrFail({
        student_id,
      });
      user.last_login = new Date();

      const newScore = new PicuEntity();
      newScore.userCheck1 = envis[0];
      newScore.userCheck2 = envis[1];
      newScore.userCheck3 = envis[2];
      newScore.userCheck4 = envis[3];
      newScore.userCheck5 = envis[4];
      newScore.userCheck6 = envis[5];
      newScore.userCheck7 = envis[6];
      newScore.userCheck8 = envis[7];
      newScore.userCheck9 = envis[8];
      newScore.userCheck10 = envis[9];
      newScore.userCheck11 = envis[10];
      newScore.userCheck12 = envis[11];
      newScore.userCheck13 = envis[12];
      newScore.userCheck14 = envis[13];
      newScore.userCheck15 = envis[14];
      newScore.userCheck16 = envis[15];
      newScore.userCheck17 = envis[16];
      newScore.userCheck18 = envis[17];
      newScore.userCheck19 = envis[18];
      newScore.userCheck20 = envis[19];
      newScore.userCheck21 = envis[20];
      newScore.stepTime1 = stepTimes[0];
      newScore.stepTime2 = stepTimes[1];
      newScore.stepTime3 = stepTimes[2];
      newScore.stepTime4 = stepTimes[3];
      newScore.stepTime5 = stepTimes[4];
      newScore.stepTime6 = stepTimes[5];
      newScore.stepTime7 = stepTimes[6];
      newScore.stepTime8 = stepTimes[7];
      newScore.stepTime9 = stepTimes[8];
      newScore.stepTime10 = stepTimes[9];
      newScore.stepTime11 = stepTimes[10];
      newScore.stepTime12 = stepTimes[11];
      newScore.stepTime13 = stepTimes[12];
      newScore.stepTime14 = stepTimes[13];
      newScore.stepTime15 = stepTimes[14];
      newScore.stepTime16 = stepTimes[15];
      newScore.stepTime17 = stepTimes[16];
      newScore.stepTime18 = stepTimes[17];
      newScore.stepTime19 = stepTimes[18];
      newScore.stepTime20 = stepTimes[19];
      newScore.stepTime21 = stepTimes[20];
      newScore.user = user;
      newScore.resultTime = resultTime;
      newScore.mode = mode;
      await this.createNewPicu(newScore);

      await this.userRepo.save(user);

      return {
        success: true,
        message: '',
      };
    } catch (error) {
      return {
        success: false,
        message: error.message,
      };
    }
  }
}
