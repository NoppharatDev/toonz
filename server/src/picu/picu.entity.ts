import { UserEntity } from 'src/user/user.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity('picus')
export class PicuEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 0 })
  userCheck1: number;

  @Column({ default: 0 })
  userCheck2: number;

  @Column({ default: 0 })
  userCheck3: number;

  @Column({ default: 0 })
  userCheck4: number;

  @Column({ default: 0 })
  userCheck5: number;

  @Column({ default: 0 })
  userCheck6: number;

  @Column({ default: 0 })
  userCheck7: number;

  @Column({ default: 0 })
  userCheck8: number;

  @Column({ default: 0 })
  userCheck9: number;

  @Column({ default: 0 })
  userCheck10: number;

  @Column({ default: 0 })
  userCheck11: number;

  @Column({ default: 0 })
  userCheck12: number;

  @Column({ default: 0 })
  userCheck13: number;

  @Column({ default: 0 })
  userCheck14: number;

  @Column({ default: 0 })
  userCheck15: number;

  @Column({ default: 0 })
  userCheck16: number;

  @Column({ default: 0 })
  userCheck17: number;

  @Column({ default: 0 })
  userCheck18: number;

  @Column({ default: 0 })
  userCheck19: number;

  @Column({ default: 0 })
  userCheck20: number;

  @Column({ default: 0 })
  userCheck21: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime1: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime2: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime3: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime4: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime5: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime6: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime7: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime8: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime9: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime10: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime11: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime12: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime13: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime14: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime15: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime16: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime17: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime18: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime19: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime20: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTime21: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  resultTime: number;

  @Column({ default: 0 })
  mode: number;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @ManyToOne((type) => UserEntity, (user) => user.picus, {
    eager: false,
  })
  user: UserEntity;
}
