import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { GameOverReturnDto } from 'src/user/dto/game-over-return.dto';
import { PicuOverDto } from 'src/user/dto/picu-over.dto';
import { PicuService } from './picu.service';

@Controller('picu')
export class PicuController {
  constructor(private picuService: PicuService) {}

  @Post('gameover')
  async picuOver(@Body() picuOverDto: PicuOverDto): Promise<GameOverReturnDto> {
    return this.picuService.picuOver(picuOverDto);
  }

  @Get('scores/:id')
  async getPicuScoreById(@Param('id') id: string) {
    return this.picuService.getPicuScoreById(id);
  }

  @Get('scores')
  async getAllPicuscores() {
    return this.picuService.getAllPicuScores();
  }
}
