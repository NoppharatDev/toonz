import { EntityRepository, Repository } from 'typeorm';
import { PicuEntity } from './picu.entity';

@EntityRepository(PicuEntity)
export class PicuRepository extends Repository<PicuEntity> {}
