export class ReturnData {
  message: string;
  statusCode: number;
}
