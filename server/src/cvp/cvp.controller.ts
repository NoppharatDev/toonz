import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { GameOverReturnDto } from 'src/user/dto/game-over-return.dto';
import { GameOverDto } from 'src/user/dto/game-over.dto';
import { CvpService } from './cvp.service';

@Controller('cvp')
export class CvpController {
  constructor(private cvpService: CvpService) {}

  // CVP
  @Post('gameover')
  async gameOver(@Body() gameOverDto: GameOverDto): Promise<GameOverReturnDto> {
    return this.cvpService.cvpOver(gameOverDto);
  }

  @Get('scores/:id')
  async getCvpScoreById(@Param('id') id: string) {
    return this.cvpService.getCvpScoreById(id);
  }

  @Get('scores')
  async getAllscores() {
    return this.cvpService.getAllCvpScores();
  }
}
