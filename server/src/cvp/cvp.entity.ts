import { UserEntity } from 'src/user/user.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity('cvps')
export class CvpEntity {
  @PrimaryGeneratedColumn()
  id: number;

  // @Column('decimal', { default: 0.0 })
  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp1: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp2: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp3: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp4: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp5: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp6: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp7: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp8: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp9: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp10: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp11: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp12: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp13: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  cvp14: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes1: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes2: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes3: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes4: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes5: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes6: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes7: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes8: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes9: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes10: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes11: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes12: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes13: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  stepTimes14: number;

  @Column()
  round: number;

  @Column('decimal', { precision: 5, scale: 2, default: 0.0 })
  resultTime: number;

  @Column({ default: 0 })
  mode: number;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @ManyToOne((type) => UserEntity, (user) => user.cvps, {
    eager: false,
  })
  user: UserEntity;
}
