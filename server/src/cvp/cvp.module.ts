import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/user/user.repository';
import { CvpController } from './cvp.controller';
import { CvpRepository } from './cvp.repository';
import { CvpService } from './cvp.service';

@Module({
  imports: [TypeOrmModule.forFeature([CvpRepository, UserRepository])],
  controllers: [CvpController],
  providers: [CvpService],
  exports: [CvpService],
})
export class CvpModule {}
