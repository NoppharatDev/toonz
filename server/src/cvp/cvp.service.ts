import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GameOverReturnDto } from 'src/user/dto/game-over-return.dto';
import { GameOverDto } from 'src/user/dto/game-over.dto';
import { UserRepository } from 'src/user/user.repository';
import { CvpEntity } from './cvp.entity';
import { CvpRepository } from './cvp.repository';

@Injectable()
export class CvpService {
  constructor(
    @InjectRepository(CvpRepository) private cvpRepo: CvpRepository,
    @InjectRepository(UserRepository) private userRepo: UserRepository,
  ) {}

  async createNewCvpScore(cvps: CvpEntity): Promise<CvpEntity> {
    try {
      const newCvp = this.cvpRepo.create(cvps);

      await this.cvpRepo.save(newCvp);

      return newCvp;
    } catch (error) {
      throw new BadRequestException();
    }
  }

  async getAllCvpScores(): Promise<any> {
    try {
      const cvpScores = await this.cvpRepo.find({ relations: ['user'] });

      return { cvpScores };
    } catch (error) {
      return {
        success: false,
        message: error.message,
      };
    }
  }

  async getCvpScoreById(id: string): Promise<any> {
    try {
      const cvpScore = await this.cvpRepo.findOneOrFail({
        where: { id: id },
        // relations: ['user'],
      });

      return { cvpScore };
    } catch (error) {
      return {
        success: false,
        message: error.message,
      };
    }
  }

  async cvpOver(gameOverDto: GameOverDto): Promise<GameOverReturnDto> {
    const { scores,stepTimes, student_id, resultTime, mode } = gameOverDto;

    if (!student_id) {
      return {
        success: false,
        message: 'โปรดระบุ student id',
      };
    }

    try {
      const user = await this.userRepo.findOneOrFail({
        student_id,
      });
      user.last_login = new Date();
      user.countRound = user.countRound + 1;

      const newScore = new CvpEntity();
      newScore.cvp1 = scores[0];
      newScore.cvp2 = scores[1];
      newScore.cvp3 = scores[2];
      newScore.cvp4 = scores[3];
      newScore.cvp5 = scores[4];
      newScore.cvp6 = scores[5];
      newScore.cvp7 = scores[6];
      newScore.cvp8 = scores[7];
      newScore.cvp9 = scores[8];
      newScore.cvp10 = scores[9];
      newScore.cvp11 = scores[10];
      newScore.cvp12 = scores[11];
      newScore.cvp13 = scores[12];
      newScore.cvp14 = scores[13];
      newScore.stepTimes1 = stepTimes[0];
      newScore.stepTimes2 = stepTimes[1];
      newScore.stepTimes3 = stepTimes[2];
      newScore.stepTimes4 = stepTimes[3];
      newScore.stepTimes5 = stepTimes[4];
      newScore.stepTimes6 = stepTimes[5];
      newScore.stepTimes7 = stepTimes[6];
      newScore.stepTimes8 = stepTimes[7];
      newScore.stepTimes9 = stepTimes[8];
      newScore.stepTimes10 = stepTimes[9];
      newScore.stepTimes11 = stepTimes[10];
      newScore.stepTimes12 = stepTimes[11];
      newScore.stepTimes13 = stepTimes[12];
      newScore.stepTimes14 = stepTimes[13];
      newScore.user = user;
      newScore.round = user.countRound;
      newScore.resultTime = resultTime;
      newScore.mode = mode;
      await this.createNewCvpScore(newScore);

      await this.userRepo.save(user);

      return {
        success: true,
        message: '',
      };
    } catch (error) {
      return {
        success: false,
        message: error.message,
      };
    }
  }
}
