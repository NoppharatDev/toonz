import { EntityRepository, Repository } from 'typeorm';
import { CvpEntity } from './cvp.entity';

@EntityRepository(CvpEntity)
export class CvpRepository extends Repository<CvpEntity> {}
