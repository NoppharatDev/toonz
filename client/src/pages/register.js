import React from "react";

const RegisterPage = () => {
  return (
    <div className="container my-3">
      {/* <a
        href="https://oauth.cmu.ac.th/v1/Authorize.aspx?response_type=code&client_id=0gskfraUz3qQMumkx2f10HRrdpC9WhyHPjDPNmuw&redirect_uri=http://localhost:4000/api/auth/callback&scope=cmuitaccount.basicinfo&state=cm"
        alt="register"
        className="btn btn-primary btn-lg"
        target="_blank"
        rel="noreferrer"
      > */}
      <a
        href="https://oauth.cmu.ac.th/v1/Authorize.aspx?response_type=code&client_id=0gskfraUz3qQMumkx2f10HRrdpC9WhyHPjDPNmuw&redirect_uri=http://157.230.33.187/api/auth/callback&scope=cmuitaccount.basicinfo&state=cm"
        alt="register"
        className="btn btn-primary btn-lg"
        target="_blank"
        rel="noreferrer"
      >
        Register
      </a>
    </div>
  );
};

export default RegisterPage;
